import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  labels: Label[]  = []
  
  constructor(public alert:AlertController) {};
  
  public appPages = [
    { title: 'Home', url: './home', icon: 'home' },
    { title: 'Puntos de Interes', url: './point-of-interest', icon: 'flag' }
  ];
  async presentAddLabel(){
    const alerta = await this.alert.create({
      cssClass: 'secondary',
      header: 'Añadir Label',  
      inputs: [{
          name: 'name',
          placeholder: 'Nombre del Label',
          id: 'nombre',
          label:'floating',   
          type: 'text',
        
        }],
        buttons:[{
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            console.log('Cancel')
          }
        },
        {
          text: 'Guardar',
          role: 'save',
          handler: (data) => {
            const label: Label = data;
            this.labels.push(label);
            
            
            console.log(this.labels);
          }
        }]
      
        

      });
    await alerta.present();

  }
  async presentDeleteLabel(index) {
    const alert = await this.alert.create({
      cssClass: 'primary',
      header: 'Delete',
      message: 'Are you sure you want to delete this label?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (cancel) => {
            console.log('Cancel');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.labels.splice(index, 1);
            console.log('Delete');
            console.log(this.labels);
          }
        }
      ]
    });

    await alert.present();
  }
}
interface Label {
  name: string;
}
