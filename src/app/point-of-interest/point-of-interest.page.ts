import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-point-of-interest',
  templateUrl: './point-of-interest.page.html',
  styleUrls: ['./point-of-interest.page.scss'],
})


export class PointOfInterestPage implements OnInit {
  
  points: Point[] = [
  ];
  
  constructor(public alert:AlertController, public labels:AppComponent ) {}
  

  ngOnInit() {
  }

  async presentAddAlert(){
    const alerta = await this.alert.create({
      cssClass: 'secondary',
      header: 'Añadir Punto',  
      inputs: [{
          name: 'name',
          placeholder: 'Nombre del punto de interés',
          id: 'nombre',
          label:'floating',   
          type: 'text',
         
        },
        {
          name: 'category',
          placeholder: 'Tus categorías ',
          id: 'categoria',
          label: 'floating',
          type: 'text',
          
        }],
        buttons:[{
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            console.log('Cancel')
          }
        },
        {
          text: 'Guardar',
          role: 'save',
          handler: (data) => {
            const point: Point = data
            this.points.push(point)
            
            
            console.log(this.points);
          }
        }]
      
        

      });
    await alerta.present();

  }
  async presentDeleteAlert(index) {
    const alert = await this.alert.create({
      cssClass: 'primary',
      header: 'Delete',
      message: 'Are you sure you want to delete this point?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (cancel) => {
            console.log('Cancel');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.points.splice(index, 1);
            console.log('Delete');
            console.log(this.points);
          }
        }
      ]
    });

    await alert.present();
  }


}

interface Point {
  name: string;
  category: string;
}
