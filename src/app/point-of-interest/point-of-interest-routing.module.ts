import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PointOfInterestPage } from './point-of-interest.page';


const routes: Routes = [
  {
    path: '',
    component: PointOfInterestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PointOfInterestPageRoutingModule {}
