import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PointOfInterestPageRoutingModule } from './point-of-interest-routing.module';

import { PointOfInterestPage } from './point-of-interest.page';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PointOfInterestPageRoutingModule,
  
  ],
  declarations: [PointOfInterestPage]
})
export class PointOfInterestPageModule {}
